//Carousel
let carousel = document.querySelector('#carousel')
carousel.StartAnimation("Appear", 1.5)

//Carousel Indicators
let carouselIndicators = document.querySelectorAll('.carousel .carousel-indicators li')
carouselIndicators.forEach( indicator => {
  indicator.addEventListener("mouseover", () => { indicator.StartAnimation("GrowUp", 1.5) })
  indicator.addEventListener("mouseout", () => { indicator.StartAnimation("GrowUp", 1) })
})

//Carousel Arrows
let arrowsIcons = document.querySelectorAll('.carousel .icon-prev, .carousel .icon-next')
arrowsIcons.forEach( icon => {
  icon.parentElement.addEventListener("mouseover", () => { icon.StartAnimation("GrowUp", 1.5) })
  icon.parentElement.addEventListener("mouseout", () => { icon.StartAnimation("GrowUp", 1) })
})
