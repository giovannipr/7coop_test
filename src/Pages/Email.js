//Envelope
let envelope = document.querySelector('.newsletter .title')
envelope.StartAnimation("Entrance", {x:-20, repeat:-1, yoyo:true}, 1)

let button = document.querySelector('.newsletter .input-group button')
button.classList.remove("glyphicon-chevron-right");
button.innerHTML = "Assinar".toUpperCase()
button.style.fontFamily = "Montserrat"
