//Grid Items
let gridItens = document.querySelectorAll('.container .grid .image')
gridItens.forEach( item => {
  item.addEventListener("mouseover", () => { item.StartAnimation("GrowUp", 1.125, .5) })
  item.addEventListener("mouseout", () => { item.StartAnimation("GrowUp", 1, .5) })
})

let gridTitles = document.querySelectorAll('.container .grid h4')
gridTitles.forEach( e => e.parentElement.style.backgroundColor = "white" )

gridTitles[0].parentElement.StartAnimation("To", {backgroundColor: "#e71d35", repeat:-1, yoyo:true}, 2)
gridTitles[0].StartAnimation("To", {opacity: 0.5, color: "yellow", repeat:-1, yoyo:true}, 2)

gridTitles[1].parentElement.StartAnimation("To", {backgroundColor: "#e71d35", repeat:-1, yoyo:true}, 2)
gridTitles[1].StartAnimation("To", {opacity: 0.5, color: "white", repeat:-1, yoyo:true}, 2)
