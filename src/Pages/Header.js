//logo
let logo = document.querySelector('.logo img')
logo.StartAnimation("Entrance", {y: 50})
logo.StartAnimation("Appear", 2)

//Hello
let hello = document.querySelector('.header .container div div div')
hello.StartAnimation("Entrance", {x: -100})
hello.StartAnimation("Appear", 0.5)

//Search Bar
let searchBar = document.querySelector('.search .input-group')
searchBar.StartAnimation("Entrance", {x: -100}, 1.5)
searchBar.StartAnimation("Appear", 0.5)

//Right Header - Cart
let cart = document.querySelector('.header .container .cart')
cart.StartAnimation("Entrance", {x: 100})
cart.StartAnimation("Appear", 0.5)

//Right Header - User Actions
let userActions = document.querySelector('.header .container .user-actions')
userActions.StartAnimation("Entrance", {x: 100})
userActions.StartAnimation("Appear", 0.5)

//Readjust Header
let mainRow = document.querySelector(".header .container .row")

let logoDiv = logo.parentElement.parentElement
logoDiv.parentNode.removeChild(logoDiv)

let formSearch = searchBar.parentElement
formSearch.parentNode.removeChild(formSearch)

let login = document.querySelector(".header > .container > .row > div > div")
login.querySelector("span").remove()
login.parentNode.removeChild(login)
console.log(login);

let onlyCart = document.querySelector(".cart i")
onlyCart.parentNode.removeChild(onlyCart)

let onlyItemsQtt = document.querySelector(".cart .cart-total-items")
onlyItemsQtt.innerHTML = "<div class='itemsCartQtt'>" + onlyItemsQtt.innerHTML.replace(/[^\d]/g, "") + "</div>"
onlyItemsQtt.parentNode.removeChild(onlyItemsQtt)

while (mainRow.firstChild) { mainRow.removeChild(mainRow.firstChild); }

mainRow.innerHTML =
SetCol(logoDiv, 5) +
SetCol(formSearch) +
SetCol(login, 2) +
SetCol("<i id='cartIcon' class='glyphicon glyphicon-shopping-cart'></i>" + onlyItemsQtt.innerHTML, 2)

function SetCol(element, size) {
  size = size || 3
  return "<div class='col-xs-" +size+ "' >" + (element.innerHTML || element) + "</div>"
}
