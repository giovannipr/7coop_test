// https://developer.mozilla.org/pt-BR/docs/Web/API/Element/insertAdjacentHTML
let menu = document.querySelector('.mnu .container .navbar')
menu.insertAdjacentHTML('beforebegin',
                        '<span id="main-menu-btn" class="input-group-btn">' +
                          '<button class="btn btn-default glyphicon glyphicon-list"></button>' +
                        '</span>'
                        );


//Animations
//Main Menu Btn
let mainMenuBtn = document.querySelector('#main-menu-btn')
mainMenuBtn.addEventListener('click', () => {
  // if( menu.style.display == 'none' ){
  if( menu.style.display == 'none' ){
    menu.style.display = 'block'
    menu.style.display = 'visible'
    menu.style.opacity = 1
    menu.StartAnimation("Appear", 0.5)
  }
  else{
    menu.StartAnimation("Desappear", {onComplete: () => {menu.style.display = 'none'} }, 0.5)
  }
})

//Items of Menu
let menuItems = document.querySelectorAll('.navbar ul li a')
menuItems.forEach( item => {
  item.addEventListener("mouseover", () => { item.StartAnimation("GrowUp", 1.125) })
  item.addEventListener("mouseout", () => { item.StartAnimation("GrowUp", 1) })

  const parent = item.parentElement
  parent.addEventListener("mouseover", () => { parent.StartAnimation("BackgroundColor", "#c0c0c0") })
  parent.addEventListener("mouseout", () => { parent.StartAnimation("BackgroundColor", "transparent") })
})
