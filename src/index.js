import './API/Animations/AnimatedElementHTML'

import './CSS/style.css'
import './CSS/menu.css'
import './CSS/redDetails.css'
import './CSS/footerStyle.css'
import './CSS/headerStyle.css' 

import './Pages/Header'
import './Pages/Navbar'
import './Pages/Carousel'
import './Pages/GridItems'
import './Pages/Email'
